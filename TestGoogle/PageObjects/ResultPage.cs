﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGoogle.PageObjects
{
    class ResultPage
    {
        private IWebDriver driver;

        public ResultPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.CssSelector, Using = "#rso > div:nth-child(2) > div > div:nth-child(4) > div > div > div.r > a:nth-child(1) > h3")]
        private IWebElement fifthArticle;

        public String GetTextOfFifthArticle()
        {
            return fifthArticle.Text;

        }
    }
}
