﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Net.NetworkInformation;
using OpenQA.Selenium.Support.UI;

namespace TestGoogle.PageObjects
{
    class SearchPage
    {
        private IWebDriver driver;
        private WebDriverWait wait; 

        public SearchPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5)); ;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "#lst-ib")]
        private IWebElement searchField;

        [FindsBy(How = How.ClassName, Using = "fusion-main-menu-icon")]
        private IWebElement searchIcon;

        [FindsBy(How = How.CssSelector, Using = "#sbtc > div.gstl_0.sbdd_a > div:nth-child(2) > div.sbdd_b > div > ul > li:nth-child(2)")]
        private IWebElement targetResult;



        public void goToPage()
        {
            driver.Navigate().GoToUrl("https://www.google.com");
        }
        public ResultPage search(string text)
        {
            searchField.SendKeys(text);
            wait.Until(ExpectedConditions.ElementToBeClickable(targetResult)).Click();
            return new ResultPage(driver);
        }


    }
}
