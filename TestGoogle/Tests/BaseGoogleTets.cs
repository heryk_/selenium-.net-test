﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using OpenQA;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Chrome;
using TestGoogle.PageObjects;

namespace TestGoogle.Tests
{
    class BaseGoogleTets
    {
        public class TestClass
        {
            private IWebDriver driver;

            [SetUp]
            public void SetUp()
            {
                driver = new ChromeDriver();
                driver.Manage().Window.Maximize();
            }

            [Test]
            public void SearchTextFromAboutPage()
            {
                SearchPage  home = new SearchPage(driver);
                home.goToPage();
                var  resultPage = home.search("selenium c#");
                Console.WriteLine(resultPage.GetTextOfFifthArticle());
                
                Assert.IsTrue(resultPage.GetTextOfFifthArticle().Contains("Selenium"));
            }

            [TearDown]
            public void TearDown()
            {
                
                driver.Close();
            }
        }
    }
}

